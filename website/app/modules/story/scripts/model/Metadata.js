class Metadata {
	constructor(id) {
		var self = this;

		self._type = "Metadata";
		self.id = id;
		self.author = "";
		self.category = "";
		self.skill = "";
		self.location = "";
		self.title = "";
	}
}