class Scene {
	constructor(name) {
		var self = this;

		self._type = "Scene";
		self.name = name;
		self.elements = [];
	}
}