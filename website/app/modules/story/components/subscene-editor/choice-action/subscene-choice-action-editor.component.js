angular
	.module("story")
	.component("subsceneChoiceActionEditor", {		
		templateUrl: "modules/story/components/subscene-editor/choice-action/subscene-choice-action-editor.html",
		bindings: {
			choiceAction: "="
		}
	});