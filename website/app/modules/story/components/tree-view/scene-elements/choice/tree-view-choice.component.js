angular
	.module("story")
	.component("treeViewChoice", {		
		templateUrl: "modules/story/components/tree-view/scene-elements/choice/tree-view-choice.html",
		bindings: {
			choice: "<"
		}
	});