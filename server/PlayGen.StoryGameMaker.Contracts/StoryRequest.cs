﻿namespace PlayGen.StoryGameMaker.Contracts
{
    public class StoryRequest
    {
		public object Metadata { get; set; }

		public object Content { get; set; }
	}
}
